package com.owc.io;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

// TODO: Auto-generated Javadoc
/**
 * The Class VerifyingInputStream.
 */
public class VerifyingInputStream extends InputStream {

    /** The Constant EOF. */
    private static final int EOF = 4;

    /** The Constant ESCAPE. */
    private static final int ESCAPE = 0;

    /** The signature method. */
    private Signature signatureMethod;

    /** The input stream. */
    private InputStream inputStream;

    /** The is signature verfied. */
    private boolean isSignatureVerfied = false;

    /**
     * Instantiates a new verifying input stream.
     *
     * @param output
     *            the output
     * @param publicKey
     *            the public key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public VerifyingInputStream(InputStream output, PublicKey publicKey) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(output, "SHA256withRSA", publicKey);
    }

    /**
     * Instantiates a new verifying input stream.
     *
     * @param inputStream
     *            the input stream
     * @param signingMethod
     *            the signing method
     * @param publicKey
     *            the public key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public VerifyingInputStream(InputStream inputStream, String signingMethod, PublicKey publicKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(inputStream, signingMethod, publicKey, null);
    }

    /**
     * Instantiates a new verifying input stream.
     *
     * @param inputStream
     *            the input stream
     * @param signingMethod
     *            the signing method
     * @param publicKey
     *            the public key
     * @param salt
     *            the salt
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public VerifyingInputStream(InputStream inputStream, String signingMethod, PublicKey publicKey, byte[] salt)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        signatureMethod = Signature.getInstance(signingMethod);
        signatureMethod.initVerify(publicKey);
        if (salt != null)
            signatureMethod.update(salt);
        this.inputStream = inputStream;
    }


    /**
     * Read.
     *
     * @return the int
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public int read() throws IOException {
        // normal entry
        int value = inputStream.read();
        // check if we reached end of content
        if (value == EOF) {
            value = inputStream.read();
            // check if we reached end of file prematurely
            if (value != EOF)
                throw new IOException("Invalid byte stream");

            // otherwise we reached signature values
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            value = inputStream.read();
            while (value != -1) {
                byteBuffer.write(value);
                value = inputStream.read();
            }
            byte[] signature = byteBuffer.toByteArray();

            try {
                if (signatureMethod.verify(signature)) {
                    isSignatureVerfied = true;
                    return -1;
                } else
                    throw new IOException("Signature invalid.");
            } catch (SignatureException e) {
                throw new IOException("Signature verification failed due to error.", e);
            }
        }

        // check if we need to escape
        if (value == ESCAPE) {
            // in this case we read the next character
            value = inputStream.read();
        }
        // check if we reached end of file prematurely
        if (value == -1)
            if (isSignatureVerfied)
                return -1;
            else
                throw new IOException("Missing Signature");
        try {
            signatureMethod.update((byte) value);
        } catch (SignatureException e) {
            throw new IOException("Signature calculation failed due to error.", e);
        }
        return value;
    }

    /**
     * Read.
     *
     * @param b
     *            the b
     * @param off
     *            the off
     * @param len
     *            the len
     * @return the int
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public int read(byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }

        int c = read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte) c;

        int i = 1;
        for (; i < len; i++) {
            c = read();
            if (c == -1) {
                break;
            }
            b[off + i] = (byte) c;
        }

        return i;
    }


    /**
     * Close.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void close() throws IOException {
        inputStream.close();
    }

    /**
     * Reset.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public synchronized void reset() throws IOException {
        inputStream.reset();
        isSignatureVerfied = false;
        try {
            signatureMethod.verify(new byte[2]);
        } catch (SignatureException e) {
        }
    }
}

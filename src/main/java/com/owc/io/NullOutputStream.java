package com.owc.io;


import java.io.IOException;
import java.io.OutputStream;

// TODO: Auto-generated Javadoc
/**
 * This just discards anything.
 * 
 * @author Sebastian Land
 *
 */
public class NullOutputStream extends OutputStream {

    /**
     * Write.
     *
     * @param b
     *            the b
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void write(int b) throws IOException {}

}

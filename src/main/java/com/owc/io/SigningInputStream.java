package com.owc.io;


import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

// TODO: Auto-generated Javadoc
/**
 * This stream reads from the given input stream and finally appends the signature once the input
 * stream finished. It uses two subsequent EOF characters to signal the end of the data stream and
 * that the next bytes will be the signature. Therefore it escapes the EOF character by a 0
 * character in the data stream. The double 0 denotes a 0 character. The signature can be checked by
 * the {@link VerifyingInputStream}.
 * 
 * @author Sebastian Land
 *
 */
public class SigningInputStream extends InputStream {

    /** The Constant EOF. */
    private static final int EOF = 4;

    /** The Constant ESCAPE. */
    private static final int ESCAPE = 0;

    /** The Constant DATA. */
    private static final int DATA = 0;

    /** The Constant ESCAPED_BYTE. */
    private static final int ESCAPED_BYTE = 1;

    /** The Constant SIGNATURE. */
    private static final int SIGNATURE = 2;

    /** The signature method. */
    private Signature signatureMethod;

    /** The input stream. */
    private InputStream inputStream;

    /** The status. */
    private int status = DATA;

    /** The escape buffer. */
    private int escapeBuffer;

    /** The signature buffer. */
    private byte[] signatureBuffer;

    /** The signature position. */
    private int signaturePosition;

    /** The salt. */
    private byte[] salt;

    /**
     * Instantiates a new signing input stream.
     *
     * @param stream
     *            the stream
     * @param privateKey
     *            the private key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningInputStream(InputStream stream, PrivateKey privateKey) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(stream, "SHA256withRSA", privateKey);
    }

    /**
     * Instantiates a new signing input stream.
     *
     * @param stream
     *            the stream
     * @param signingMethod
     *            the signing method
     * @param privateKey
     *            the private key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningInputStream(InputStream stream, String signingMethod, PrivateKey privateKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(stream, signingMethod, privateKey, null);
    }

    /**
     * Instantiates a new signing input stream.
     *
     * @param stream
     *            the stream
     * @param signingMethod
     *            the signing method
     * @param privateKey
     *            the private key
     * @param salt
     *            the salt
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningInputStream(InputStream stream, String signingMethod, PrivateKey privateKey, byte[] salt)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        signatureMethod = Signature.getInstance(signingMethod);
        signatureMethod.initSign(privateKey);
        this.salt = salt;
        if (salt != null)
            signatureMethod.update(salt);
        this.inputStream = stream;
    }


    /**
     * Close.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void close() throws IOException {
        if (status != SIGNATURE && signaturePosition != signatureBuffer.length)
            throw new IOException("Closed before signing");
        inputStream.close();
    }

    /**
     * Reset.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public synchronized void reset() throws IOException {
        inputStream.reset();
        try {
            signatureMethod.sign();
            if (salt != null)
                signatureMethod.update(salt);
        } catch (SignatureException e) {
            throw new IOException("Could not reset signature", e);
        }
    }

    /**
     * Read.
     *
     * @return the int
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public int read() throws IOException {
        switch (status) {
            case DATA:
                int readByte = inputStream.read();
                try {
                    if (readByte != -1) {
                        // stream still does deliver results
                        signatureMethod.update((byte) readByte);
                        if (readByte == ESCAPE || readByte == EOF) {
                            status = ESCAPED_BYTE;
                            escapeBuffer = readByte;
                            return ESCAPE;
                        } else {
                            return readByte;
                        }
                    } else {
                        // stream ended
                        status = SIGNATURE;
                        signatureBuffer = signatureMethod.sign();

                        signaturePosition = -2; // to indicate that we have to return another EOF
                                                // first
                        return EOF;
                    }
                } catch (Exception e) {
                    throw new IOException("Could not sign data.", e);
                }
            case ESCAPED_BYTE:
                status = DATA;
                return escapeBuffer;
            default: // signature
                signaturePosition++;
                if (signaturePosition < 0)
                    return EOF;
                else if (signaturePosition < signatureBuffer.length) {
                    return signatureBuffer[signaturePosition] & 0xFF;
                } else
                    return -1;
        }

    }
}

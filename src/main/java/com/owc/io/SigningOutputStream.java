package com.owc.io;


import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

// TODO: Auto-generated Javadoc
/**
 * The Class SigningOutputStream.
 */
public class SigningOutputStream extends OutputStream {

    /** The Constant EOF. */
    private static final int EOF = 4;

    /** The Constant ESCAPE. */
    private static final int ESCAPE = 0;

    /** The signature method. */
    private Signature signatureMethod;

    /** The output stream. */
    private OutputStream outputStream;

    /**
     * Instantiates a new signing output stream.
     *
     * @param output
     *            the output
     * @param privateKey
     *            the private key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningOutputStream(OutputStream output, PrivateKey privateKey) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(output, "SHA256withRSA", privateKey);
    }

    /**
     * Instantiates a new signing output stream.
     *
     * @param outputStream
     *            the output stream
     * @param signingMethod
     *            the signing method
     * @param privateKey
     *            the private key
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningOutputStream(OutputStream outputStream, String signingMethod, PrivateKey privateKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        this(outputStream, signingMethod, privateKey, null);
    }

    /**
     * Instantiates a new signing output stream.
     *
     * @param outputStream
     *            the output stream
     * @param signingMethod
     *            the signing method
     * @param privateKey
     *            the private key
     * @param salt
     *            the salt
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws SignatureException
     *             the signature exception
     */
    public SigningOutputStream(OutputStream outputStream, String signingMethod, PrivateKey privateKey, byte[] salt)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        signatureMethod = Signature.getInstance(signingMethod);
        signatureMethod.initSign(privateKey);
        if (salt != null)
            signatureMethod.update(salt);
        this.outputStream = outputStream;
    }

    /**
     * Write.
     *
     * @param b
     *            the b
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void write(int b) throws IOException {
        try {
            signatureMethod.update((byte) b);
        } catch (SignatureException e) {
            throw new IOException("Signing exception.", e);
        }

        if (b == EOF || b == ESCAPE) {
            outputStream.write(ESCAPE);
        }
        outputStream.write(b);
    }

    /**
     * Close.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void close() throws IOException {
        try {
            byte[] signature = signatureMethod.sign();
            outputStream.write(EOF);
            outputStream.write(EOF);
            outputStream.write(signature);
            outputStream.flush();
        } catch (SignatureException e) {
            throw new IOException("Signing exception.", e);
        }
        super.close();
    }
}

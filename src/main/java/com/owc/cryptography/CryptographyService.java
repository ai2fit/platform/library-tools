package com.owc.cryptography;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.owc.cryptography.exceptions.SpecificNoSuchAlgorithmException;
import com.owc.cryptography.exceptions.SpecificNoSuchPaddingException;
import com.owc.tools.Base64Tools;

// TODO: Auto-generated Javadoc
/**
 * The Class CryptographyService.
 */
public class CryptographyService {

    /** The Constant DEFAULT_CHARSET. */
    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /** The Constant SIGNATURE_METHOD_PROPERTY. */
    public static final String SIGNATURE_METHOD_PROPERTY = "opa_tad.signature_method";

    /** The Constant DEFAULT_SIGNATURE_METHOD. */
    public static final String DEFAULT_SIGNATURE_METHOD = "SHA256withRSA";

    /** The Constant SIGNATURE_ALGORITHM_PROPERTY. */
    public static final String SIGNATURE_ALGORITHM_PROPERTY = "opa_tad.signature_algorithm";

    /** The Constant DEFAULT_SIGNATURE_ALGORITHM. */
    public static final String DEFAULT_SIGNATURE_ALGORITHM = "RSA";

    /** The Constant SIGNATURE_MIN_KEY_LENGTH. */
    public static final int SIGNATURE_MIN_KEY_LENGTH = 2048;

    /** The Constant HASH_METHOD_PROPERTY. */
    public static final String HASH_METHOD_PROPERTY = "opa_tad.hash_method";

    /** The Constant DEFAULT_HASH_METHOD. */
    public static final String DEFAULT_HASH_METHOD = "SHA-256";

    /** The Constant ASYMMETRIC_ALGORITHM_PROPERTY. */
    public static final String ASYMMETRIC_ALGORITHM_PROPERTY = "opa_tad.asymmetric_algorithm";

    /** The Constant ASYMMETRIC_METHOD_PROPERTY. */
    public static final String ASYMMETRIC_METHOD_PROPERTY = "opa_tad.asymmetric_method";

    /** The Constant DEFAULT_ASYMMETRIC_ALGORITHM. */
    public static final String DEFAULT_ASYMMETRIC_ALGORITHM = "RSA"; // "ECDSA"

    /** The Constant DEFAULT_ASYMMETRIC_ENCODING_METHOD. */
    public static final String DEFAULT_ASYMMETRIC_ENCODING_METHOD = "RSA/ECB/OAEPWithSHA1AndMGF1Padding"; // "ECDSA/ECB/OAEPWithSHA1AndMGF1Padding"

    /** The Constant ASYMMETRIC_MIN_KEY_LENGTH. */
    public static final int ASYMMETRIC_MIN_KEY_LENGTH = 2048;

    /** The Constant SYMMETRIC_ALGORITHM_PROPERTY. */
    public static final String SYMMETRIC_ALGORITHM_PROPERTY = "opa_tad.symmetric_algorithm";

    /** The Constant SYMMETRIC_METHOD_PROPERTY. */
    public static final String SYMMETRIC_METHOD_PROPERTY = "opa_tad.symmetric_method";

    /** The Constant DEFAULT_SYMMETRIC_ALGORITHM. */
    public static final String DEFAULT_SYMMETRIC_ALGORITHM = "AES";

    /** The Constant DEFAULT_SYMMETRIC_METHOD. */
    public static final String DEFAULT_SYMMETRIC_METHOD = "AES/GCM/NoPadding";

    /** The Constant SYMMETRIC_MIN_KEY_LENGTH. */
    public static final int SYMMETRIC_MIN_KEY_LENGTH = 256;

    /**
     * This method will derive a public key using the current cryptography algorithms of this
     * instance.
     *
     * @param base64EncodedKey
     *            the base 64 encoded key
     * @return the public key
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws InvalidKeySpecException
     *             the invalid key spec exception
     */
    public static PublicKey derivePublicKey(String base64EncodedKey) throws SpecificNoSuchAlgorithmException, InvalidKeySpecException {
        try {
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64Tools.decode(base64EncodedKey));
            KeyFactory keyFactory = KeyFactory.getInstance(getAsymmetricMethod());
            return keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getAsymmetricMethod(), ASYMMETRIC_ALGORITHM_PROPERTY, DEFAULT_ASYMMETRIC_ALGORITHM);
        }
    }

    /**
     * This method will derive a public key using the current cryptography algorithms of this
     * instance.
     *
     * @param base64EncodedKey
     *            the base 64 encoded key
     * @return the private key
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws InvalidKeySpecException
     *             the invalid key spec exception
     */
    public static PrivateKey derivePrivateKey(String base64EncodedKey) throws SpecificNoSuchAlgorithmException, InvalidKeySpecException {
        try {
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64Tools.decode(base64EncodedKey));
            KeyFactory keyFactory = KeyFactory.getInstance(getAsymmetricMethod());
            return keyFactory.generatePrivate(publicKeySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getAsymmetricMethod(), ASYMMETRIC_ALGORITHM_PROPERTY, DEFAULT_ASYMMETRIC_ALGORITHM);
        }
    }

    /**
     * This method returns the currently configured MessageDigest method for use in the project.
     *
     * @return the message digest
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     */
    public static MessageDigest deriveMessageDigest() throws SpecificNoSuchAlgorithmException {
        try {
            return MessageDigest.getInstance(getHashMethod());
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getHashMethod(), HASH_METHOD_PROPERTY, DEFAULT_HASH_METHOD);
        }
    }

    /**
     * This returns the id of the currently configured cryptography method for symmetric
     * cryptography.
     *
     * @return the symmetric algorithm
     */
    private static String getSymmetricAlgorithm() {
        String method = System.getProperty(SYMMETRIC_ALGORITHM_PROPERTY);
        if (method == null)
            return DEFAULT_SYMMETRIC_ALGORITHM;
        return method;
    }

    /**
     * This returns the id of the currently configured cryptography method for symmetric
     * cryptography.
     *
     * @return the symmetric method
     */
    private static String getSymmetricMethod() {
        String method = System.getProperty(SYMMETRIC_METHOD_PROPERTY);
        if (method == null)
            return DEFAULT_SYMMETRIC_METHOD;
        return method;
    }

    /**
     * This returns the configured minimal key length for symmetric encryption. It is used for
     * generating new keys.
     *
     * @return the symmetric key length
     */
    private static int getSymmetricKeyLength() {
        return SYMMETRIC_MIN_KEY_LENGTH;
    }

    /**
     * This returns the id of the currently configured cryptography method for asymmetric
     * cryptography.
     *
     * @return the asymmetric method
     */
    private static String getAsymmetricMethod() {
        String method = System.getProperty(ASYMMETRIC_ALGORITHM_PROPERTY);
        if (method == null)
            return DEFAULT_ASYMMETRIC_ALGORITHM;
        return method;
    }

    /**
     * This returns the configured minimal key length for encryption. It is used for generating new
     * keys.
     *
     * @return the asymmetric key length
     */
    private static int getAsymmetricKeyLength() {
        return ASYMMETRIC_MIN_KEY_LENGTH;
    }

    /**
     * This returns the configured minimal key length for signatures. It is used for generating new
     * keys.
     *
     * @return the signature key length
     */
    private static int getSignatureKeyLength() {
        return SIGNATURE_MIN_KEY_LENGTH;
    }

    /**
     * This returns the id of the currently configured asymmetric encoding method.
     *
     * @return the asymmetric encoding method
     */
    private static String getAsymmetricEncodingMethod() {
        String method = System.getProperty(ASYMMETRIC_METHOD_PROPERTY);
        if (method == null)
            return DEFAULT_ASYMMETRIC_ENCODING_METHOD;
        return method;
    }

    /**
     * This returns a cipher initialized for encoding with the given public key.
     *
     * @param publicKey
     *            the public key
     * @return the cipher
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher deriveAsymmetricEncodingCipher(PublicKey publicKey)
            throws InvalidKeyException, SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        try {
            Cipher cipher = Cipher.getInstance(getAsymmetricEncodingMethod());
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher;
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getAsymmetricEncodingMethod(), ASYMMETRIC_METHOD_PROPERTY, DEFAULT_ASYMMETRIC_ENCODING_METHOD);
        } catch (NoSuchPaddingException e) {
            throw new SpecificNoSuchPaddingException(e, getAsymmetricEncodingMethod(), ASYMMETRIC_METHOD_PROPERTY, DEFAULT_ASYMMETRIC_ENCODING_METHOD);
        }
    }

    /**
     * This returns a cipher initialized for encoding with the given public key.
     *
     * @param privateKey
     *            the private key
     * @return the cipher
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher deriveAsymmetricDecodingCipher(PrivateKey privateKey)
            throws InvalidKeyException, SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        try {
            Cipher cipher = Cipher.getInstance(getAsymmetricEncodingMethod());
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher;
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getAsymmetricEncodingMethod(), ASYMMETRIC_METHOD_PROPERTY, DEFAULT_ASYMMETRIC_ENCODING_METHOD);
        } catch (NoSuchPaddingException e) {
            throw new SpecificNoSuchPaddingException(e, getAsymmetricEncodingMethod(), ASYMMETRIC_METHOD_PROPERTY, DEFAULT_ASYMMETRIC_ENCODING_METHOD);
        }
    }

    /**
     * This tests if the public encoding key of a user is compatible with this platforms
     * requirements.
     *
     * @param publicKey
     *            the public key
     * @return true, if is public encoding key compatible
     */
    public static boolean isPublicEncodingKeyCompatible(PublicKey publicKey) {
        return getKeyLength(publicKey) >= ASYMMETRIC_MIN_KEY_LENGTH;
    }

    /**
     * This returns the id of the currently configured hash method.
     *
     * @return the hash method
     */
    private static String getHashMethod() {
        String method = System.getProperty(HASH_METHOD_PROPERTY);
        if (method == null)
            return DEFAULT_HASH_METHOD;
        return method;
    }

    /**
     * This returns a new Signature object initiated with a public key for verifying signatures
     * using the platforms configured method.
     *
     * @param signaturePublicKey
     *            the signature public key
     * @return the signature
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws InvalidKeyException
     *             the invalid key exception
     */
    public static Signature deriveSignatureVerifier(PublicKey signaturePublicKey) throws SpecificNoSuchAlgorithmException, InvalidKeyException {
        try {
            Signature instance = Signature.getInstance(getSignatureMethod());
            instance.initVerify(signaturePublicKey);
            return instance;
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getSignatureMethod(), SIGNATURE_METHOD_PROPERTY, DEFAULT_SIGNATURE_METHOD);
        }
    }

    /**
     * This returns a new Signature object initiated with a private key for building signatures
     * using the platforms configured method.
     *
     * @param signaturePrivateKey
     *            the signature private key
     * @return the signature
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws InvalidKeyException
     *             the invalid key exception
     */
    public static Signature deriveSignatureSigner(PrivateKey signaturePrivateKey) throws SpecificNoSuchAlgorithmException, InvalidKeyException {
        try {
            Signature instance = Signature.getInstance(getSignatureMethod());
            instance.initSign(signaturePrivateKey);
            return instance;
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getSignatureMethod(), SIGNATURE_METHOD_PROPERTY, DEFAULT_SIGNATURE_METHOD);
        }
    }

    /**
     * This tests if the signature key of a user is compatible with this platforms requirements.
     *
     * @param signaturePublicKey
     *            the signature public key
     * @return true, if is signature key compatible
     */
    public static boolean isSignatureKeyCompatible(PublicKey signaturePublicKey) {
        return getKeyLength(signaturePublicKey) >= SIGNATURE_MIN_KEY_LENGTH;
    }

    /**
     * This returns the id of the currently configured signature method.
     *
     * @return the signature method
     */
    private static String getSignatureMethod() {
        String method = System.getProperty(SIGNATURE_METHOD_PROPERTY);
        if (method == null)
            return DEFAULT_SIGNATURE_METHOD;
        return method;
    }

    /**
     * This returns the id of the currently configured signature algorithm.
     *
     * @return the signature algorithm
     */
    private static String getSignatureAlgorithm() {
        String method = System.getProperty(SIGNATURE_ALGORITHM_PROPERTY);
        if (method == null)
            return DEFAULT_SIGNATURE_ALGORITHM;
        return method;
    }

    /**
     * Gets the key length of supported keys.
     *
     * @param pk
     *            PublicKey used to derive the keysize
     * @return -1 if key is unsupported, otherwise a number greater or equal 0. 0 usually means the
     *         length can not be calculated, for example if the key is an EC key and the
     *         "implicitlyCA" encoding is used.
     */
    public static int getKeyLength(final PublicKey pk) {
        int len = -1;
        if (pk instanceof RSAPublicKey) {
            final RSAPublicKey rsapub = (RSAPublicKey) pk;
            len = rsapub.getModulus().bitLength();
        } else if (pk instanceof ECPublicKey) {
            final ECPublicKey ecpriv = (ECPublicKey) pk;
            final java.security.spec.ECParameterSpec spec = ecpriv.getParams();
            if (spec != null) {
                len = spec.getOrder().bitLength(); // does this really return something we expect?
            } else {
                // We support the key, but we don't know the key length
                len = 0;
            }
        } else if (pk instanceof DSAPublicKey) {
            final DSAPublicKey dsapub = (DSAPublicKey) pk;
            if (dsapub.getParams() != null) {
                len = dsapub.getParams().getP().bitLength();
            } else {
                len = dsapub.getY().bitLength();
            }
        }
        return len;
    }

    /**
     * Generate asymmetric key pair.
     *
     * @return the key pair
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     */
    public static KeyPair generateAsymmetricKeyPair() throws SpecificNoSuchAlgorithmException {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(getAsymmetricMethod());
            keyPairGenerator.initialize(getAsymmetricKeyLength());
            return keyPairGenerator.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getAsymmetricMethod(), ASYMMETRIC_ALGORITHM_PROPERTY, DEFAULT_ASYMMETRIC_ALGORITHM);
        }
    }

    /**
     * Generate signature key pair.
     *
     * @return the key pair
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     */
    public static KeyPair generateSignatureKeyPair() throws SpecificNoSuchAlgorithmException {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(getSignatureAlgorithm());
            keyPairGenerator.initialize(getSignatureKeyLength());
            return keyPairGenerator.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getSignatureAlgorithm(), SIGNATURE_ALGORITHM_PROPERTY, DEFAULT_SIGNATURE_ALGORITHM);
        }
    }

    /**
     * This creates a new secret key for the given byte array using the configured symmetric
     * encryption method.
     *
     * @param key
     *            the key
     * @return the secret key
     */
    public static SecretKey deriveSymmetricSecret(byte[] key) {
        return new SecretKeySpec(key, getSymmetricAlgorithm());
    }

    /**
     * This returns a cipher initialized for symmetric encoding with the given secret key.
     *
     * @param secretKey
     *            the secret key
     * @return the cipher
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher deriveSymmetricEncodingCipher(SecretKey secretKey) throws SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        try {
            return Cipher.getInstance(getSymmetricMethod());
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getSymmetricMethod(), SYMMETRIC_METHOD_PROPERTY, DEFAULT_SYMMETRIC_METHOD);
        } catch (NoSuchPaddingException e) {
            throw new SpecificNoSuchPaddingException(e, getSymmetricMethod(), SYMMETRIC_METHOD_PROPERTY, DEFAULT_SYMMETRIC_METHOD);
        }
    }

    /**
     * This returns a cipher initialized for symmetric decoding with the given secret key.
     *
     * @param secretKey
     *            the secret key
     * @return the cipher
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher deriveSymmetricDecodingCipher(SecretKey secretKey) throws SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        try {
            return Cipher.getInstance(getSymmetricMethod());
        } catch (NoSuchAlgorithmException e) {
            throw new SpecificNoSuchAlgorithmException(e, getSymmetricMethod(), SYMMETRIC_METHOD_PROPERTY, DEFAULT_SYMMETRIC_METHOD);
        } catch (NoSuchPaddingException e) {
            throw new SpecificNoSuchPaddingException(e, getSymmetricMethod(), SYMMETRIC_METHOD_PROPERTY, DEFAULT_SYMMETRIC_METHOD);
        }
    }

    /**
     * Inits the symmetric encoding cipher.
     *
     * @param cipher
     *            the cipher
     * @param key
     *            the key
     * @param currentRowIV
     *            the current row IV
     * @return the cipher
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws InvalidAlgorithmParameterException
     *             the invalid algorithm parameter exception
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher initSymmetricEncodingCipher(Cipher cipher, SecretKey key, byte[] currentRowIV)
            throws InvalidKeyException, InvalidAlgorithmParameterException, SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        if (cipher == null)
            cipher = deriveSymmetricEncodingCipher(key);
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, currentRowIV);
        cipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec);
        return cipher;
    }

    /**
     * Inits the symmetric decoding cipher.
     *
     * @param cipher
     *            the cipher
     * @param key
     *            the key
     * @param currentRowIV
     *            the current row IV
     * @return the cipher
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws InvalidAlgorithmParameterException
     *             the invalid algorithm parameter exception
     * @throws SpecificNoSuchAlgorithmException
     *             the specific no such algorithm exception
     * @throws SpecificNoSuchPaddingException
     *             the specific no such padding exception
     */
    public static Cipher initSymmetricDecodingCipher(Cipher cipher, SecretKey key, byte[] currentRowIV)
            throws InvalidKeyException, InvalidAlgorithmParameterException, SpecificNoSuchAlgorithmException, SpecificNoSuchPaddingException {
        if (cipher == null)
            cipher = deriveSymmetricDecodingCipher(key);
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, currentRowIV);
        cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
        return cipher;
    }

}

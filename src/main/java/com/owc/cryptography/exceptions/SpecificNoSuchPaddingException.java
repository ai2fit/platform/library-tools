package com.owc.cryptography.exceptions;


import javax.crypto.NoSuchPaddingException;

// TODO: Auto-generated Javadoc
/**
 * The Class SpecificNoSuchPaddingException.
 */
public class SpecificNoSuchPaddingException extends NoSuchPaddingException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The algorithm. */
    private final String algorithm;

    /** The environment variable. */
    private final String environmentVariable;

    /** The default algorithm. */
    private final String defaultAlgorithm;

    /**
     * Instantiates a new specific no such padding exception.
     *
     * @param e
     *            the e
     * @param algorithm
     *            the algorithm
     * @param environmentVariable
     *            the environment variable
     * @param defaultAlgorithm
     *            the default algorithm
     */
    public SpecificNoSuchPaddingException(NoSuchPaddingException e, String algorithm, String environmentVariable, String defaultAlgorithm) {
        super(e.getMessage());

        this.algorithm = algorithm;
        this.environmentVariable = environmentVariable;
        this.defaultAlgorithm = defaultAlgorithm;
    }

    /**
     * Gets the algorithm.
     *
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * Gets the environment variable.
     *
     * @return the environment variable
     */
    public String getEnvironmentVariable() {
        return environmentVariable;
    }

    /**
     * Gets the default algorithm.
     *
     * @return the default algorithm
     */
    public String getDefaultAlgorithm() {
        return defaultAlgorithm;
    }
}

package com.owc.cryptography;


import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.owc.tools.Base64Tools;
import com.owc.tools.RandomTools;

// TODO: Auto-generated Javadoc
/**
 * This is a class for storing the keys necessary to communicate with OPA_TAD instance.
 *
 * @author sland
 */
public class PersistentKeyRing implements Serializable {

    /** The Constant SALT. */
    private static final byte[] SALT = "OldSecureWorld!".getBytes(Charset.forName("utf-8"));

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5760022663045908877L;

    /** The public key. */
    PublicKey publicKey;

    /** The private key. */
    PrivateKey privateKey;

    /** The public signature key. */
    PublicKey publicSignatureKey;

    /** The private signature key. */
    PrivateKey privateSignatureKey;

    /** The hashing salt. */
    String hashingSalt;

    /**
     * Instantiates a new persistent key ring.
     *
     * @param publicEncryptionKey
     *            the public encryption key
     * @param privateEncryptionKey
     *            the private encryption key
     * @param publicSignatureKey
     *            the public signature key
     * @param privateSignatureKey
     *            the private signature key
     * @param hashingSalt
     *            the hashing salt
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws InvalidKeySpecException
     *             the invalid key spec exception
     */
    public PersistentKeyRing(String publicEncryptionKey, String privateEncryptionKey, String publicSignatureKey, String privateSignatureKey, String hashingSalt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        this(CryptographyService.derivePublicKey(publicEncryptionKey), CryptographyService.derivePrivateKey(privateEncryptionKey),
                CryptographyService.derivePublicKey(publicSignatureKey), CryptographyService.derivePrivateKey(privateSignatureKey), hashingSalt);
    }

    /**
     * Instantiates a new persistent key ring.
     *
     * @param publicEncryptionKey
     *            the public encryption key
     * @param privateEncryptionKey
     *            the private encryption key
     * @param publicSignatureKey
     *            the public signature key
     * @param privateSignatureKey
     *            the private signature key
     * @param hashingSalt
     *            the hashing salt
     */
    public PersistentKeyRing(PublicKey publicEncryptionKey, PrivateKey privateEncryptionKey, PublicKey publicSignatureKey, PrivateKey privateSignatureKey,
            String hashingSalt) {
        this.publicKey = publicEncryptionKey;
        this.privateKey = privateEncryptionKey;
        this.publicSignatureKey = publicSignatureKey;
        this.privateSignatureKey = privateSignatureKey;
        this.hashingSalt = hashingSalt;
    }

    /**
     * Gets the hashing salt as string.
     *
     * @return the hashing salt as string
     */
    public String getHashingSaltAsString() {
        return hashingSalt;
    }

    /**
     * Gets the private signature key.
     *
     * @return the private signature key
     */
    public PrivateKey getPrivateSignatureKey() {
        return privateSignatureKey;
    }

    /**
     * This returns the public key in base 64 encoding.
     *
     * @return the public key as string
     */
    public String getPublicKeyAsString() {
        return Base64Tools.encode(publicKey.getEncoded());
    }

    /**
     * Gets the private key.
     *
     * @return the private key
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * Gets the public key.
     *
     * @return the public key
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * Gets the public signature key.
     *
     * @return the public signature key
     */
    public PublicKey getPublicSignatureKey() {
        return publicSignatureKey;
    }

    /**
     * This returns the public key for building signatures in base 64 encoding.
     *
     * @return the public signature key as string
     */
    public String getPublicSignatureKeyAsString() {
        return Base64Tools.encode(publicSignatureKey.getEncoded());
    }

    /**
     * Load.
     *
     * @param in
     *            the in
     * @param password
     *            the password
     * @return the persistent key ring
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws NoSuchPaddingException
     *             the no such padding exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws InvalidAlgorithmParameterException
     *             the invalid algorithm parameter exception
     * @throws InvalidKeySpecException
     *             the invalid key spec exception
     */
    public static PersistentKeyRing load(InputStream in, char[] password) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
            IOException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        // derive key
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password, SALT, 65536, 256);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");

        // read version (now only 1)
        in.read();

        // read IV
        int length = in.read();
        if (length > 0) {
            byte[] iv = new byte[length];
            for (int i = 0; i < length; i++) {
                int readByte = in.read();
                if (readByte > 0)
                    iv[i] = (byte) readByte;
                else {
                    throw new IOException("Illegal key file.");
                }
            }

            // decrypt keys
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv); // 128 bit auth tag
                                                                            // length
            cipher.init(Cipher.DECRYPT_MODE, secret, parameterSpec);

            try (ObjectInputStream ois = new ObjectInputStream(new CipherInputStream(in, cipher))) {
                return (PersistentKeyRing) ois.readObject();
            } catch (ClassNotFoundException e) {
                throw new IOException("Class not found. Check your setup or restart application after updates.", e);
            }
        } else {
            throw new IOException("Illegal key file.");
        }
    }

    /**
     * This stores the key p.
     *
     * @param pair
     *            the pair
     * @param out
     *            the out
     * @param password
     *            the password
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws NoSuchPaddingException
     *             the no such padding exception
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws InvalidKeySpecException
     *             the invalid key spec exception
     * @throws InvalidParameterSpecException
     *             the invalid parameter spec exception
     * @throws IllegalBlockSizeException
     *             the illegal block size exception
     * @throws BadPaddingException
     *             the bad padding exception
     * @throws InvalidAlgorithmParameterException
     *             the invalid algorithm parameter exception
     */
    public static void store(PersistentKeyRing pair, OutputStream out, char[] password)
            throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, InvalidParameterSpecException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        // derive key
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password, SALT, 65536, 256);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");

        // encrypt keys
        byte[] iv = new byte[12];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv); // 128 bit auth tag length
        cipher.init(Cipher.ENCRYPT_MODE, secret, parameterSpec);

        // writing
        // version
        out.write(1);

        // first write IV's length as byte
        out.write((byte) iv.length);
        // then write IV
        out.write(iv);

        // then write object
        try (ObjectOutputStream ois = new ObjectOutputStream(new CipherOutputStream(out, cipher))) {
            ois.writeObject(pair);
        }
    }

    /**
     * This method return a newly generated key pair. Please notice that the returned keys have 2048
     * bit, hence a compatible JCE provider must be installed. Don't use this for an existing user
     * account. Using new keys will destroy access of your data. You should store the keys in a
     * secure place using {@link #store(PersistentKeyRing, OutputStream, char[])}.
     *
     * @return the persistent key ring
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     */
    public static PersistentKeyRing generate() throws NoSuchAlgorithmException {
        KeyPair keyPair = CryptographyService.generateAsymmetricKeyPair();
        KeyPair signatureKeyPair = CryptographyService.generateSignatureKeyPair();
        return new PersistentKeyRing(keyPair.getPublic(), keyPair.getPrivate(), signatureKeyPair.getPublic(), signatureKeyPair.getPrivate(),
                RandomTools.generateRandomString(50));
    }

    /**
     * To bytes.
     *
     * @param chars
     *            the chars
     * @return the byte[]
     */
    static byte[] toBytes(char[] chars) {
        CharBuffer charBuffer = CharBuffer.wrap(chars);
        ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
        byte[] bytes = Arrays.copyOfRange(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
        return bytes;
    }

    /**
     * Hash code.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((privateKey == null) ? 0 : privateKey.hashCode());
        result = prime * result + ((publicKey == null) ? 0 : publicKey.hashCode());
        return result;
    }

    /**
     * Equals.
     *
     * @param obj
     *            the obj
     * @return true, if successful
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PersistentKeyRing other = (PersistentKeyRing) obj;
        if (privateKey == null) {
            if (other.privateKey != null)
                return false;
        } else if (!privateKey.equals(other.privateKey))
            return false;
        if (publicKey == null) {
            if (other.publicKey != null)
                return false;
        } else if (!publicKey.equals(other.publicKey))
            return false;
        return true;
    }

}

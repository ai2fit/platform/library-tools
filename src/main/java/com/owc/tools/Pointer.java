package com.owc.tools;

// TODO: Auto-generated Javadoc
/**
 * This is a class for having a traversable pointer of a given type. It uses an long index as
 * backend and provides methods for going forward and backwars.
 *
 * @author Sebastian Land
 * @param <T>
 *            the generic type
 */
public abstract class Pointer<T> {

    /** The index. */
    private long index = 0;

    /**
     * Gets the.
     *
     * @param index
     *            the index
     * @return the t
     */
    protected abstract T get(long index);

    /**
     * Peek forward.
     *
     * @return the t
     */
    public T peekForward() {
        return get(index + 1);
    }

    /**
     * Forward.
     *
     * @return the t
     */
    public T forward() {
        index++;
        return get(index);
    }

    /**
     * Backward.
     *
     * @return the t
     */
    public T backward() {
        index--;
        return get(index);
    }

    /**
     * Peek backward.
     *
     * @return the t
     */
    public T peekBackward() {
        return get(index - 1);
    }

    /**
     * Gets the.
     *
     * @return the t
     */
    public T get() {
        return get(index);
    }
}

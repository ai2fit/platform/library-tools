package com.owc.tools;


import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

// TODO: Auto-generated Javadoc
/**
 * The Class Base64Tools.
 */
public class Base64Tools {

    /** The encoder. */
    private static Encoder encoder = Base64.getEncoder();

    /** The decoder. */
    private static Decoder decoder = Base64.getDecoder();

    /**
     * Encode.
     *
     * @param value
     *            the value
     * @return the string
     */
    public static String encode(int value) {
        return encode(intToByteArray(value));
    }

    /**
     * Encode.
     *
     * @param value
     *            the value
     * @return the string
     */
    public static String encode(byte[] value) {
        return encoder.encodeToString(value);
    }

    /**
     * Decode.
     *
     * @param value
     *            the value
     * @return the byte[]
     */
    public static byte[] decode(String value) {
        return decoder.decode(value);
    }

    /**
     * Clean.
     *
     * @param value
     *            the value
     * @return the string
     */
    public static String clean(String value) {
        return encode(decode(value));
    }

    /**
     * Int to byte array.
     *
     * @param value
     *            the value
     * @return the byte[]
     */
    public static final byte[] intToByteArray(int value) {
        return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
    }
}

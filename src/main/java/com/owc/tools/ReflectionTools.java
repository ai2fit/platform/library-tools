package com.owc.tools;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessControlException;
import java.util.Arrays;

public class ReflectionTools {

    /**
     * This method tests whether the given object is assignable to the given class by using
     * reflection to cross borders of different class loaders like the PluginClassLoaders of
     * RapidMiner. The comparison is done using the fully qualified class name.
     *
     * @param object
     * @param clazz
     * @return true if the object is an instance of the class, otherwise false
     */
    public static boolean isObjectInstanceOf(Object object, Class<?> clazz) {
        String className = clazz.getName();
        try {
            Class<?> localClassEquivalent = object.getClass().getClassLoader().loadClass(className);
            return localClassEquivalent.isInstance(object);
        } catch (ClassNotFoundException | AccessControlException ignored) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * This method will execute the specified method on the object with the given parameters.
     *
     * @param object
     * @param methodName
     * @param params
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static Object executeMethod(Object object, String methodName, Object... params)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        // find right method
        Method[] methods = object.getClass().getMethods();
        for (Method method : methods) {
            if (methodName.equals(method.getName())) {
                // now test parameters
                Class<?>[] methodParameterTypes = method.getParameterTypes();
                if (params.length == methodParameterTypes.length) {
                    boolean matches = true;
                    for (int i = 0; i < params.length; i++) {
                        if (!methodParameterTypes[i].isInstance(params[i])) {
                            matches = false;
                            break;
                        }
                    }

                    if (matches) {
                        return method.invoke(object, params);
                    }
                }
            }
        }

        // throw exception
        String[] parameterTypes = Arrays.stream(params).map(o -> o.getClass().getName()).toArray(String[]::new);
        throw new IllegalArgumentException("No method found with name " + methodName + " and signature " + Arrays.toString(parameterTypes));
    }

}

package com.owc.tools;

// TODO: Auto-generated Javadoc
/**
 * The Class ByteTools.
 */
public class ByteTools {

    /**
     * Double to bytes.
     *
     * @param x
     *            the x
     * @return the byte[]
     */
    public static byte[] doubleToBytes(double x) {
        return longToBytes(Double.doubleToRawLongBits(x));
    }

    /**
     * Long to bytes.
     *
     * @param x
     *            the x
     * @return the byte[]
     */
    public static byte[] longToBytes(long x) {
        return new byte[] { (byte) ((x >> 56) & 0xff), (byte) ((x >> 48) & 0xff), (byte) ((x >> 40) & 0xff), (byte) ((x >> 32) & 0xff),
                (byte) ((x >> 24) & 0xff), (byte) ((x >> 16) & 0xff), (byte) ((x >> 8) & 0xff), (byte) ((x >> 0) & 0xff), };
    }

    /**
     * Int to bytes.
     *
     * @param x
     *            the x
     * @return the byte[]
     */
    public static byte[] intToBytes(int x) {
        return new byte[] { (byte) ((x >> 24) & 0xff), (byte) ((x >> 16) & 0xff), (byte) ((x >> 8) & 0xff), (byte) ((x >> 0) & 0xff), };
    }

}

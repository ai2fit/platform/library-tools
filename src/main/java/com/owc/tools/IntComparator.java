package com.owc.tools;

public interface IntComparator {

    public int compare(int x, int y);

}

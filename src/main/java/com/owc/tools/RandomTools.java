
package com.owc.tools;


import java.util.Random;

// TODO: Auto-generated Javadoc
/**
 * The Class RandomTools.
 */
public class RandomTools {

    /**
     * This method returns a random string consisting only of Characters and numbers.
     *
     * @param length
     *            the length
     * @return the random string
     */
    public static String generateRandomString(int length) {
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        for (int i = 0; i < length; i++) {
            if (random.nextBoolean()) {
                // then create character
                int randomLimitedInt = leftLimit + random.nextInt(rightLimit - leftLimit + 1);
                if (random.nextBoolean()) {
                    // as lower case
                    buffer.append((char) randomLimitedInt);
                } else {
                    buffer.append(Character.toUpperCase((char) randomLimitedInt));
                }
            } else {
                // then create number
                buffer.append(random.nextInt(10));
            }
        }
        return buffer.toString();
    }
}

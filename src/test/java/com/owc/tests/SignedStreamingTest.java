package com.owc.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import org.junit.jupiter.api.Test;

import com.owc.cryptography.CryptographyService;
import com.owc.io.SigningOutputStream;
import com.owc.io.VerifyingInputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class SignedStreamingTest.
 */
public class SignedStreamingTest {

    /**
     * Copies the contents read from the input stream to the output stream in the current thread.
     * Both streams will be closed, even in case of a failure.
     *
     * @param in
     *            the in
     * @param out
     *            the out
     * @param closeOutputStream
     *            the close output stream
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void copyStreamSynchronously(InputStream in, OutputStream out, boolean closeOutputStream) throws IOException {
        byte[] buffer = new byte[1024 * 20];
        try {
            int length;
            while ((length = in.read(buffer)) != -1) {
                out.write(buffer, 0, length);
            }
            out.flush();
        } finally {
            if (closeOutputStream && out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                }
            }
        }

    }

    /**
     * Test verifying input stream.
     *
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws SignatureException
     *             the signature exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    void testVerifyingInputStream() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
        KeyPair signatureKeyPair = CryptographyService.generateSignatureKeyPair();

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        SigningOutputStream signOS = new SigningOutputStream(os, signatureKeyPair.getPrivate());
        for (int i = 0; i < 2048; i++) {
            signOS.write(i % 256);
            signOS.write(i % 256);
        }
        signOS.close();
        byte[] signedByteArray = os.toByteArray();

        VerifyingInputStream verifyingIS = new VerifyingInputStream(new ByteArrayInputStream(signedByteArray), signatureKeyPair.getPublic());
        for (int i = 0; i < 2048; i++) {
            assertEquals(i % 256, verifyingIS.read());
            assertEquals(i % 256, verifyingIS.read());
        }
        while (verifyingIS.read() != -1) {}
        ;
        while (verifyingIS.read() != -1) {}
        ;

        verifyingIS.reset();
        for (int i = 0; i < 2048; i++) {
            assertEquals(i % 256, verifyingIS.read());
            assertEquals(i % 256, verifyingIS.read());
        }
        while (verifyingIS.read() != -1) {}

        while (verifyingIS.read() != -1) {}


        verifyingIS.reset();
        os = new ByteArrayOutputStream();
        copyStreamSynchronously(verifyingIS, os, true);
        byte[] byteArray = os.toByteArray();
        int cell = 0;
        for (int i = 0; i < 2048; i++) {
            assertEquals((byte) i % 256, byteArray[cell++]);
            assertEquals((byte) i % 256, byteArray[cell++]);
        }
    }
}
